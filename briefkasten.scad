use </Users/marchoppe/Documents/3d/OpenSCAD/libraries/screw-library/dpscrew.scad>
use </Users/marchoppe/Documents/3d/OpenSCAD/libraries/Round-Anything/polyround.scad>
use </Users/marchoppe/Documents/3d/openscad-designs/lib/shortcuts.scad>

use </Users/marchoppe/Documents/3d/openscad-designs/lib/mhop_lib.scad>

*color("grey") translate([30,0,0]) rotate([0,90,0])
    import("/Users/marchoppe/Downloads/Simple arcade door lock - 4170860/files/doorlock-body-v1.0.1.stl");

$fn=80;

d_gew=18;
pitch_gew=2;
d_2=13;
d_i=8.9;
d_i2=6.5;
b=14.5;
l_gew=9; // Länge Gewinde
l=14; // Länge Bolzen

gap=0.3;
h=4; // Dicke des Knebelbleches

dblock=1.0; // Höhe Nut


heb_b=2.9; // Kreuzbalken

ha_l=2; // Dicke des hebelblechs
M=3; // Durchmesser Schraube Befestigung Hebel
round_i=4; // Knob


bolt_h=5;
bolt_h2=1.5;
bolt_d1=d_gew+3;
bolt_d2=d_gew+5;
bolt_x=3;
bolt_y=2;

//Tx(-30)
*translate([0,0,h+gap]) gewindeteil();
//Ty(30) 
Innenteil();
*Tz(h+2) Bolt();

module Bolt() {
    difference() {
         union() {
            cylinder(h=bolt_h,  r=bolt_d1/2);
            cylinder(h=bolt_h2, r=bolt_d2/2);
            for(w = [0 : 45 : 360]) Rz(w)
                Tx(bolt_d2/2-bolt_x-0.05) Ty(-bolt_y/2)
                    cube([bolt_x, bolt_y, bolt_h+1]);
         }
         screw(size = d_gew, length = bolt_h, pitch = pitch_gew, profile = 1);
    }
}


 module Nut() {
     MMxy() MM(-1,1,0) {
    rotate_extrude(angle=90) Tx(d_gew/2) circle(dblock+gap); 
    Tx(d_gew/2) sphere(dblock+gap);
     }
}


module gewindeteil() {
 difference() {
    union() {
        points2=[
            [0,0,0],
            [0,-h,0],
            [-(d_gew/2+2.5),-h,2],
            [-(d_gew/2+5),-1,1],
            [-(d_gew/2+5),0,0.1]
        ];
        rotate_extrude() polygon(polyRound(points2,30));
        cylinder(l,r=d_2/2);
        Tz(-1)
            screw(size = d_gew, length = l_gew, pitch = pitch_gew, profile = 3);
    }
    MMy() translate([-30/2,b/2]) cube([30,10,40]);
    cylinder(l*3,r=d_i/2+gap, center=true);
    Tz(-(h-gap/2)) Nut();    
 }
} 
 

 module Innenteil() {
    knob_pts = [
        [0,0,0],
        [d_gew/2+2,0,round_i],
        [d_gew/2+2,d_i,round_i],
        [0,d_i,0]
    ];
    difference() {
        union() {
            cylinder(l+h+gap*2, r=d_i/2); // Bolzen
            Tz(gap/2) cube([d_i*13/10, d_i, gap], center=true);
            Ty(-d_i/2) Rx(-90)
            rotate_extrude(angle=180) polygon ( polyRound(knob_pts) );
            // Gegenstück Nut
            MMx() Tx(d_gew/2) hull() { 
                sphere(dblock); 
                Tz(dblock/2+gap)sphere(dblock);
            }
            Tz(l+h) hebel_aufnahme();
        }
        Tz(l+h+gap-10+ha_l) {
            #screw(size = M, length = 10, pitch = 1.25, profile = 2);
        }
    }
}


module hebel_aufnahme() {
    linear_extrude(ha_l) union() {
        intersection() {
            circle(r=d_i/2);
            heb_pts = [
                [0,0],
                [d_i, 0],
                [d_i, heb_b/2],
                [heb_b/2,heb_b/2],
                [heb_b/2,d_i],
                [0,d_i]
            ];
            union() {
                MMy() MMx() polygon(heb_pts);
         
            }
        }
        circle(r=d_i2/2);
    }
}

 // Nut to go with this screw
 //translate([30, 0, 0])
 *difference() {
    cylinder(d = 16, h = 6, $fn=6);
    *translate([0,0,-2]) screw(size = M, length = 2*6, pitch = 1.25, profile = 2);
 }

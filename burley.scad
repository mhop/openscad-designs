use </Users/marchoppe/Documents/3d/OpenSCAD/libraries/Round-Anything/polyround.scad>

use </Users/marchoppe/Documents/3d/openscad-designs/lib/mhop_lib.scad>
use </Users/marchoppe/Documents/3d/openscad-designs/lib/shortcuts.scad>


rrad=160;
brad=45;
d=5;
roben=24;
rkant=5;
$fn=80;

wink=brad*sin(11);


knubbel_b=11;
knubbel_ri=11.5;
knubbel_ra=18;
knubbel_raa=28;


dom_ri=2.6/2;
dom_rs=6.5/2;
dom_ra=20/2;
dom_b=8;




//MMz() Tz(30) 
{
    *color("gray") //Tx(13) Tz(-460) Ty(-290) 
    T(425,-315,14) Ry(180) import("/Users/marchoppe/Documents/3d/openscad-designs/thingiverse/Kotfl_gel_f_r_Burley_Travoy_____mud_fender_for_Burley_Travoy_3854237/files/1Kotflügel_Links.stl");

    // Blech
    points2=[
        [0,0,rkant],
        [rrad,0, roben],
        [rrad+wink,brad, rkant],
        [rrad+wink-d, brad, rkant],
        [rrad-d,d, roben-d/2],
        [0,d,rkant]
    ];
    difference() {
        Rz(-90) rotate_extrude(angle=270) polygon(polyRound(points2,50));
        // Überstände weg
        Tx(-rrad-knubbel_raa) Cu(rrad*2,rrad*3,brad*2);
        Rz(-3) Ty(-rrad-knubbel_raa) Cu(rrad*3,rrad*2,brad*2);
        // Loch Achse
        Tz(-knubbel_b+d/2) cylinder(r=11.5, h=2*knubbel_b+0.1);
        // Dome Löcher
        Ty(81.5) Tz(-0.1)cylinder(d=dom_rs*2, h=5*d);
        Ty(-23)
        Tx(144) Tz(-0.1) cylinder(d=dom_rs*2, h=5*d);
    }
    Tx(-3.5) Ty(-24) Rz(-3) Rx(90) polyRoundExtrude(points2,d, 1.5, 0);
    Tz(0) Tx(-29) Ty(-2) Rz(90) Rx(90) polyRoundExtrude(points2,d, 0, 1.5 );
        
    points3=[
        [knubbel_ra,0,0],
        [knubbel_ra, d, 0],
        [knubbel_raa+1.15, d, 1.6],
        [knubbel_raa+1.15, 0, 1.6]
    ];
    rotate_extrude() polygon(polyRound(points3,50));

    dome();
    
    // Knubbel Achse
    knubbel_pts=[
        [knubbel_ri,0,0],
        [knubbel_ri, knubbel_b, 0],
        [knubbel_ra, knubbel_b, 8],
        [knubbel_raa, d/2, 0],
        [knubbel_raa, 0, 0]
    ];
    *polygon(polyRound(knubbel_pts,55));
    Tz(d/2) MMz() rotate_extrude() polygon(polyRound(knubbel_pts,50));
    
}
    
    
    
module dome()
{
    dom_pts=[
        [dom_ri,0,0.2],
        [dom_ri, dom_b-2, 0.3],
        [dom_rs, dom_b-2, 0],
        [dom_rs, dom_b, 0.5],
        [dom_ra, dom_b, 4],
        [dom_ra, 0, 3]
    ];
    *polygon(polyRound(dom_pts,55));
    Ty(81.5) rotate_extrude() polygon(polyRound(dom_pts,50));
    Ty(-23) Tx(144) rotate_extrude() polygon(polyRound(dom_pts,50));
}
